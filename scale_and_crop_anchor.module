<?php

/**
 * @file
 * Scale and Crop Anchor module file.
 */

/**
 * Implements hook_image_effect_info_alter().
 */
function scale_and_crop_anchor_image_effect_info_alter(&$effects) {
  if (!empty($effects['image_scale_and_crop'])) {
    $effects['image_scale_and_crop']['effect callback'] = 'scale_and_crop_anchor_image_scale_and_crop_effect';
    $effects['image_scale_and_crop']['form callback'] = 'image_crop_form';
    $effects['image_scale_and_crop']['summary theme'] = 'image_crop_summary';
  }
}

/**
 * Image effect callback; Scale and crop an image resource (with an anchor).
 *
 * @param $image
 *   An image object returned by image_load().
 * @param $data
 *   An array of attributes to use when performing the scale and crop effect
 *   with the following items:
 *   - "width": An integer representing the desired width in pixels.
 *   - "height": An integer representing the desired height in pixels.
 *   - "anchor": A string describing where the crop should originate in the form
 *     of "XOFFSET-YOFFSET". XOFFSET is either a number of pixels or
 *     "left", "center", "right" and YOFFSET is either a number of pixels or
 *     "top", "center", "bottom".
 * @return
 *   TRUE on success. FALSE on failure to scale and crop image.
 * @see scale_and_crop_anchor_image_scale_and_crop()
 */
function scale_and_crop_anchor_image_scale_and_crop_effect(&$image, $data) {
  // Set sane default values.
  $data += array(
    'anchor' => 'center-center',
  );

  if (!scale_and_crop_anchor_image_scale_and_crop($image, $data['width'], $data['height'], $data['anchor'])) {
    watchdog('image', 'Image scale and crop failed using the %toolkit toolkit on %path (%mimetype, %dimensions)', array('%toolkit' => $image->toolkit, '%path' => $image->source, '%mimetype' => $image->info['mime_type'], '%dimensions' => $image->info['width'] . 'x' . $image->info['height']), WATCHDOG_ERROR);
    return FALSE;
  }
  return TRUE;
}

/**
 * Scales an image to the exact width and height given.
 *
 * This function achieves the target aspect ratio by cropping the original image
 * according to the selected anchor type.
 *
 * The resulting image always has the exact target dimensions.
 *
 * @param $image
 *   An image object returned by image_load().
 * @param $width
 *   The target width, in pixels.
 * @param $height
 *   The target height, in pixels.
 * @param $anchor
 *   The anchor to use when cropping.
 *
 * @return
 *   TRUE on success, FALSE on failure.
 *
 * @see image_load()
 * @see image_resize()
 * @see image_crop()
 * @see scale_and_crop_anchor_image_scale_and_crop_effect()
 */
function scale_and_crop_anchor_image_scale_and_crop(stdClass $image, $width, $height, $anchor = 'center-center') {
  $scale = max($width / $image->info['width'], $height / $image->info['height']);

  // Set the top left coordinates of the crop area, based on the anchor.
  list($x, $y) = explode('-', $anchor);
  $x = image_filter_keyword($x, $image->info['width'] * $scale, $width);
  $y = image_filter_keyword($y, $image->info['height'] * $scale, $height);

  if (image_resize($image, $image->info['width'] * $scale, $image->info['height'] * $scale)) {
    return image_crop($image, $x, $y, $width, $height);
  }
  return FALSE;
}
